module.exports = function(db) {
  return {
    /**
     * Retrieves access token to be used for authentication
     */
    getAccessToken: function () {
      return {
        accessToken: 'accesstoken'
      };
    },

    /**
     * Saves the token that was just created
     * @param userId
     * @param client
     * @param authorizationCode
     * @param scope
     */
    saveToken: function(token, client, userId) {
      var model = db.OauthToken.build();

      model.token = token;
      model.user_id = userId;

      return model.save().then(function(model) {
        return {
          accessToken: model.token.accessToken,
          client: client,
          user: userId
        };
      });
    },

    /**
     * Retrieves data about a given client ID.  Information about a
     * client must be entered ahead of time.
     * @param clientId
     * @returns
     */
    getClient: function (clientId) {
      // Some DB stuff here
      // This stuff needs to be built by the database
      return db.OauthClientInformation
        // client_id is the primary key, the 'id'
        .findById(clientId)
        .then(function(information) {
          return {
            clientId: information.client_id,
            clientSecret: information.client_secret,
            grants: information.grants,
            redirectUris: information.redirect_uris
          };
        }).catch(function() {
          // Need to catch errors here
        });

    },

    /**
     * Generates an access token to be used for
     * @returns {string}
     *
     * Server generates it's own if this is not provided
     */
    // generateAccessToken: function () {
    //   return 'accesstoken';
    // },

    /**
     * Can be used to generate an authorization code for use on the
     * authorization leg an OAuth2 request.
     * @returns {string}
     *
     * Server generates it's own if this is not provided
     */
    // generateAuthorizationCode: function () {
    //   return 'authorizationcode';
    // },

    /**
     * Generates a refresh token
     * @returns {string}
     *
     * Server generates a random token if this is not provided
     */
    // generateRefreshToken: function () {
    //   return 'refreshtoken';
    // },


    /**
     * Sets the expiration on the current pending authorization such
     * that it's invalidated.
     */
    revokeAuthorizationCode: function (code) {
      return db.OauthPendingAuthorization.findOne({
        where: {
          code: {
            $contains: {
              authorizationCode: code.authorizationCode
            }
          }
        }
      }).then(function(pending) {
        if (!pending) {
          throw "Unable to revoke authorization code"
        }

        pending.code.expiresAt = new Date();
        pending.save();

        return {
          expiresAt: pending.code.expiresAt
        };
      }).catch(function() {
        return null;
      })
    },

    /**
     * Gives an opportunity for storing the authorization code that
     * providers will use for the next leg of the authentication.
     * @param {object} code An object containing the following properties,
     * authorizationCode, expiresAt, redirectUri, scope
     * @param {object} client
     * @param {object} user
     * @returns {{authorizationCode: string}} Must return the given
     * authorization code
     */
    saveAuthorizationCode: function (code, client, user) {
      var model = db.OauthPendingAuthorization.build();

      model.expiration = code.expiresAt;
      model.user_id = parseInt(user.id);
      model.code = code;
      model.client = client;

      return model.save().then(function() {
        return {
          authorizationCode: code.authorizationCode
        }
      });
    },

    /**
     * Retrieves code and parameters that were saved during the first
     * leg of the oauth2 cycle
     * @param authorizationCode
     */
    getAuthorizationCode: function(authorizationCode) {
      console.log('CODE', authorizationCode);
      return db.OauthPendingAuthorization.findOne({
        where: {
          code: {
            $contains: {
              authorizationCode: authorizationCode
            }
          }
        }
      }).then(function(pending) {
        if (!pending) {
          throw "Authorization code not found"
        }
        var obj = pending.code;
        obj.client = pending.client;
        obj.user = pending.user_id;

        return obj;
      }).catch(function() {
        return null;
      });
    }

    /**
     * Not sure what this does but it should validate that the given
     * scope matches the given scope.
     *
     * There are no scopes
     */
    // validateScope: function () {
    //   return true;
    // }
  }
};