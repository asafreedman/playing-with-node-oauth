var crypto = require('crypto');

var pbkdfConfig = {
  saltBytes: 12,
  hashBytes: 256,
  iterations: 77
};

module.exports = function (db) {
  /**
   * Intercepts the authorization handler from doing its own thing.
   *
   * @param request
   * @param response
   * @returns {{user: {name: string}}}
   */
  this.handle = function (req, res) {
    if (!req.query.credentials) {
      return null;
    }
    return db.Account
      .findOne({where: {username: req.query.credentials.username}})
      .then(function (account) {
        if (!account) {
          throw "No Account Found";
        }

        return account;
      }).then(function (account) {
        var key = crypto.pbkdf2Sync(
          req.query.credentials.password,
          new Buffer(account.pw_salt, 'base64'),
          pbkdfConfig.iterations,
          pbkdfConfig.hashBytes);

        if (key.toString('hex') == account.pw_hash) {
          return account;
        }

        throw "Incorrect password";
      }).then(function (account) {
        return {
          id: account.id,
          username: account.username
        };
      }).catch(function () {
        // This forces an error
        return null;
      });
  };

  return this;
};