'use strict';
var table = 'app.oauth_client_information';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(table, {
      client_id: {
        type: Sequelize.TEXT,
        primaryKey: true
      },
      client_secret: {
        type: Sequelize.TEXT
      },
      grants: {
        type: Sequelize.JSON
      },
      redirect_uris: {
        type: Sequelize.JSON
      },
      created_at: {
        type: Sequelize.DATE
      }
    }, {
      charset: 'utf8'
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable(table)
  }
};
