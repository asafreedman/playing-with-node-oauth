'use strict';

var table = 'app.oauth_pending_authorizations';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.createTable(table, {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      code: {
        type: Sequelize.JSONB
      },
      client: {
        type: Sequelize.JSONB
      },
      expiration: {
        type: Sequelize.DATE
      },
      user_id: {
        type: Sequelize.BIGINT
      }
    });
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.dropTable(table);
  }
};
