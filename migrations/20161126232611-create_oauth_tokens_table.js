'use strict';

var table = 'app.oauth_tokens';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.createTable(table, {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      token: {
        type: Sequelize.JSONB
      },
      user_id: {
        type: Sequelize.BIGINT
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.dropTable(table);
  }
};
