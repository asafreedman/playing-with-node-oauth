var fs = require('fs');
var db = require('./models');
var https = require('https');
var express = require('express');
var expressLogging = require('express-logging');
var logger = require('logops');
var bodyParser = require('body-parser');
var env = require('./.env.json');
var app = express();

// static file serving
app.use(express.static('static'));

// LOGGING
app.use(expressLogging(logger));

// Message body parsing... because it's extra... req.body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./routes')(app, db);

if (env.server.https) {
    // For use with the LetsEncrypt service
    var ca = [], cert = [];
    var chain = fs.readFileSync(env.server.caFile, 'utf8');

    chain = chain.split("\n");

    for(var lineNum = 0; lineNum < chain.length; lineNum++) {
        cert.push(chain[lineNum]);
        if (chain[lineNum].match(/-END CERTIFICATE-/)) {
            ca.push(cert.join("\n"));
            cert = []
        }
    }

    var options = {
        ca: ca,
        key: fs.readFileSync(env.server.privKeyFile),
        cert: fs.readFileSync(env.server.certFile)
    };

    https.createServer(options, app).listen(443);
} else {
    app.listen(80);
}
