var OAuthServer = require('express-oauth-server');
var path = require('path');
var pug = require('pug');

module.exports = function (app, db) {
  var oauth = new OAuthServer({
    // Needs to be fleshed out
    model: require('../handlers/authenticationModel')(db),
    // Will need to be fleshed out
    // Only used for authorize route,
    // Probably too long
    accessTokenLifetime: 3600 * 24 * 365,
    // Possible insecure
    allowBearerTokensInQueryString: true,
    // Possibly insecure
    // http://www.twobotechnologies.com/blog/2014/02/
    // importance-of-state-in-oauth2.html
    allowEmptyState: true,
    // Suppress built in error handler and give it to 'next'
    useErrorHandler: true
  });

  // test
  app.get('/hello-world', function (req, res) {
    res.send('Hello, world!');
  });

  // OAuth2 Token
  app.post('/token', oauth.token());

  // Oauth2 Authorize
  // Authentorizes if the credentials are right
  app.get('/authorize', oauth.authorize({
    authenticateHandler: require('../handlers/authenticateHandler')(db)
  }));
  // Allows the user to log in
  app.use('/authorize', function(err, req, res, next) {
    var data = {
      client_id: req.query.client_id,
      redirect_uri: req.query.redirect_uri,
      response_type: req.query.response_type,
      state: req.query.state
    };

    return res.send(pug.renderFile(path.join(__dirname, '/../views/login.pug'), data));
  });
};
