module.exports = function(sequelize, DataTypes) {
  var OauthPendingAuthorization = sequelize.define('OauthPendingAuthorization', {
    code: {
      type: DataTypes.JSONB,
      get: function() {
        var obj = this.getDataValue('code');
        obj.expiresAt = new Date(obj.expiresAt);
        return obj;
      }
    },
    client: DataTypes.JSONB,
    expiration: DataTypes.DATE,
    user_id: DataTypes.INTEGER
  }, {
    timestamps: false,
    tableName: 'oauth_pending_authorizations',
    schema: 'app',
    schemaDelimiter: '.'
  });

  return OauthPendingAuthorization;
};