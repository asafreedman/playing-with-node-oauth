module.exports = function(sequelize, DataTypes) {
  var OauthToken = sequelize.define('OauthToken', {
    token: DataTypes.JSONB,
    user_id: DataTypes.INTEGER
  }, {
    timestamps: false,
    tableName: 'oauth_tokens',
    schema: 'app',
    schemaDelimiter: '.'
  });

  return OauthToken;
};