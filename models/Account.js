
module.exports = function(sequelize, DataTypes) {
  var Account = sequelize.define('Account', {
    "username": {
      type: DataTypes.TEXT
    },
    "pw_salt": {
      type: DataTypes.STRING
    },
    "pw_hash": {
      type: DataTypes.STRING
    },
    "disabled": {
      type: DataTypes.BOOLEAN
    }
  }, {
    classMethods: {

    },
    timestamps: false,
    tableName: 'account',
    schema: 'app',
    schemaDelimiter: '.'
  });

  return Account;
};