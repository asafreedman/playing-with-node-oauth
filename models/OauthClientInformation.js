module.exports = function(sequelize, DataTypes) {
  var OauthClientInformation = sequelize.define('OauthClientInformation', {
    "client_id": {
      type: DataTypes.STRING,
      primaryKey: true
    },
    "client_secret": DataTypes.STRING,
    "grants": DataTypes.JSON,
    "redirect_uris": DataTypes.JSON,
    "created_at": DataTypes.DATE
  }, {
    classMethods: {

    },
    timestamps: false,
    tableName: 'oauth_client_information',
    schema: 'app',
    schemaDelimiter: '.'
  });

  return OauthClientInformation;
};