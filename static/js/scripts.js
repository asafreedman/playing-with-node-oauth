$(function() {
  $('form input').on('keyup', function() {
    var validForm = [].slice.call(document.getElementsByTagName('input')).every(function(el) {
      if (el.willValidate) {
        return el.validity.valid;
      } else {
        return true;
      }
    });

    if (validForm) {
      $('button').removeAttr('disabled');
    } else {
      $('button').attr('disabled', 'disabled');
    }
  })
});